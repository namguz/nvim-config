return {
  { -- Filesystem explorer
    "preservim/nerdtree",

    config = function()
      -- Key remap
      vim.keymap.set("n", "<leader>tt", ":NERDTreeToggle<CR>", { desc = "[T]ree [T]oggle" })
      vim.keymap.set("n", "<leader>tf", ":NERDTreeFocus<CR>", { desc = "[T]ree [F]ocus" })
      vim.keymap.set("n", "<leader>tc", ":NERDTreeFind<CR>", { desc = "[T]ree find [C]urrent" })
    end,
  },
}
-- vim: ts=2 sts=2 sw=2 et
